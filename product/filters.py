from django import forms
from django.utils.translation import ugettext_lazy as _
import django_filters as filters

from .models import Product, Brand, Category


class ProductFilter(filters.FilterSet):
    name = filters.CharFilter(
        lookup_expr='iexact',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    brand = filters.ModelChoiceFilter(
        queryset=Brand.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    category__parent = filters.ModelChoiceFilter(
        label=_('Categoria'),
        queryset=Category.objects.filter(parent__isnull=True),
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    category = filters.ModelChoiceFilter(
        label=_('Sub-Categoria'),
        queryset=Category.objects.filter(parent__isnull=False),
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    price__gt = filters.NumberFilter(
        label=_('Preço mínimo'),
        name='price',
        lookup_expr='gt',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    price__lt = filters.NumberFilter(
        label=_('Preço máximo'),
        name='price',
        lookup_expr='lt',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Product
        exclude = ['slug', 'unity', 'price', 'image']
