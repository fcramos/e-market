from uuid import uuid4
from django.db import models
from django.utils.translation import ugettext_lazy as _


def get_upload_path(instance, filename):
    if isinstance(instance, Brand):
        folder = 'product_brands'

    elif isinstance(instance, Product):
        folder = 'product_pictures'

    else:
        folder = 'product'

    ext = filename.split('.')[-1]
    return '%s/%s.%s' % (folder, uuid4().hex, ext)


class Brand(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    slug = models.SlugField(_('slug'))
    logo = models.ImageField(_('logomarca'), upload_to=get_upload_path, blank=True, null=True)

    class Meta:
        ordering = ['name']
        verbose_name = _('marca')
        verbose_name_plural = _('marcas')

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    slug = models.SlugField(_('slug'))
    parent = models.ForeignKey('self', verbose_name=_('categoria pai'), null=True, blank=True)

    class Meta:
        ordering = ['name']
        verbose_name = _('categoria')
        verbose_name_plural = _('categorias')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    slug = models.SlugField(_('slug'))
    brand = models.ForeignKey(Brand, verbose_name=_('marca'))
    category = models.ForeignKey(Category, verbose_name=_('categoria'))
    unity = models.CharField(_('unidade'), max_length=2)
    price = models.DecimalField(_('preço unitário'), decimal_places=2, max_digits=15)
    image = models.ImageField(_('imagem'), upload_to=get_upload_path, blank=True, null=True)

    class Meta:
        ordering = ['name', 'brand']
        verbose_name = _('produto')
        verbose_name_plural = _('produtos')

    def __str__(self):
        return '{} {}'.format(self.name, self.brand.name)

    def category_path(self):
        category = self.category
        path = [category.name]

        while category.parent:
            path.append(category.parent.name)
            category = category.parent

        return ' > '.join(reversed(path))
