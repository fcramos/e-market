from django.contrib import admin

from product.models import Brand, Category, Product


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ['name', 'logo']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'brand', 'category', 'price']
    list_filter = ['brand', 'category']
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {'fields': (('name', 'slug'),)}),
        ('Informações', {'fields': (
            ('category', 'brand'),
            ('unity', 'price'),
            ('image',)
        )})
    )

    def get_field_queryset(self, db, db_field, request):
        if db_field.name == 'category':
            return Category.objects.filter(parent__isnull=False)
        else:
            return super(ProductAdmin, self).get_field_queryset(db, db_field, request)
