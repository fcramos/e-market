from django.test import TestCase

from product.models import Brand, Category, Product


class BrandTestCase(TestCase):
    def setUp(self):
        self.brand = Brand(
            name='Dell'
        )
        self.brand.save()

    def test_create(self):
        'Brand may be saved'
        self.assertEqual(1, self.brand.pk)


class CategoryTestCase(TestCase):
    def setUp(self):
        self.parent_category = Category(
            name='Parent'
        )
        self.parent_category.save()

        self.category = Category(
            name='XPTO',
            parent=self.parent_category
        )
        self.category.save()

    def test_create(self):
        'Category may be saved'
        self.assertEqual(1, self.category.pk)

    def test_parent_category(self):
        'Category may have a parent category'
        self.assertEqual(self.parent_category, self.category.parent)


class ProductTestCase(TestCase):
    def setUp(self):
        brand = Brand(
            name='Ajax'
        )
        brand.save()

        category = Category(
            name='General'
        )
        category.save()

        self.product = Product(
            name='Some product',
            slug='some-product',
            brand=brand,
            category=category,
            unity='PÇ',
            price=12.99
        )
        self.product.save()

    def test_create(self):
        'Product may be saved'
        self.assertEqual(1, self.product.pk)
