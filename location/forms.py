from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import State, City


class LocationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        if instance:
            kwargs.update({
                'initial': {
                    'state': instance.city.state,
                }
            })
            for city in instance.city.state.city_set.all():
                self.base_fields['city'].widget.choices.append(
                    (city.pk, city.name)
                )

        super(LocationForm, self).__init__(*args, **kwargs)

    state = forms.ModelChoiceField(
        label=_('Estado'),
        queryset=State.objects.all()
    )
    city = forms.IntegerField(
        label=_('Cidade'),
        widget=forms.Select
    )

    class Media:
        js = (
            '/static/admin/js/fix-jquery.js',
            '/static/location/js/state-cities.js',
        )

    def clean_city(self):
        return City.objects.get(pk=self.data.get('city'))
