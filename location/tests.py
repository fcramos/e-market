from django.test import TestCase

from location.models import State, City


class StateTest(TestCase):
    def setUp(self):
        self.state = State(
            name='Rio de Janeiro',
            initials='RJ'
        )
        self.state.save()

    def test_create(self):
        'State may be saved'
        self.assertEqual(1, self.state.pk)

    def test_str(self):
        'State string representation may be the name.'
        self.assertEqual('Rio de Janeiro', str(self.state))


class CityTest(TestCase):
    def setUp(self):
        self.state = State(
            name='Rio de Janeiro',
            initials='RJ'
        )
        self.state.save()

        self.city = City(
            name='Petropolis',
            state=self.state
        )
        self.city.save()

    def test_create(self):
        'City may be saved'
        self.assertEqual(1, self.city.pk)

    def test_str(self):
        'City string representation may be the name.'
        self.assertEqual('Petropolis', str(self.city))

    def test_state(self):
        'City may have a state'
        self.assertEqual(self.state, self.city.state)
