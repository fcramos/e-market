from django.db import models
from django.utils.translation import ugettext_lazy as _


class State(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    initials = models.CharField(_('sigla'), max_length=2)

    class Meta:
        ordering = ['name']
        verbose_name = _('Estado')
        verbose_name_plural = _('Estados')

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    state = models.ForeignKey(State)

    class Meta:
        ordering = ['state', 'name']
        verbose_name = _('Cidade')
        verbose_name_plural = _('Cidades')

    def __str__(self):
        return self.name
