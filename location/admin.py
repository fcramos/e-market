# coding: utf-8
from django.contrib import admin

from location.models import State, City


class CityInline(admin.TabularInline):
    model = City
    fields = ['name']
    extra = 1


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    search_fields = ['name', 'initials']
    list_display = (
        'name',
        'initials'
    )
    inlines = [CityInline]


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ['name', 'state']
    list_filter = ['state']
    list_display = (
        'name',
        'state'
    )
