from django.conf.urls import url

from .views import CityList

urlpatterns = [
    url(r'^cities', CityList.as_view(), name='cities'),
]
