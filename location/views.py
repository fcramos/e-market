from django.http.response import JsonResponse
from django.views.generic import ListView

from .models import City


class CityList(ListView):
    model = City

    def get_queryset(self):
        if self.request.GET.get('state'):
            return City.objects.filter(
                state_id=self.request.GET.get('state')
            )
        else:
            return City.objects.all()

    def get(self, request, *args, **kwargs):
        cities = []
        for city in self.get_queryset():
            cities.append({
                'id': city.pk,
                'name': city.name
            })

        return JsonResponse({'results': cities})
