$(document).ready(function(){

  $('#id_state').change(function(){
    if(!this.value){
      return;
    }

    $.get('/location/cities', {state: this.value}, function(data){
      $('#id_city').empty().append($('<option/>').html('---------'));

      $.each(data.results, function(index, city) {
        $('#id_city').append(
          $('<option/>').val(city.id).html(city.name)
        );
      });

    });
  });
});
