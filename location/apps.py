from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'location'
    verbose_name = 'Estados e cidades'
