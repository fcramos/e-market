from django.apps import AppConfig


class MyAuthConfig(AppConfig):
    name = 'account'
    verbose_name = 'Autenticação e Autorização'
