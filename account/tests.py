from django.test import TestCase
from django.db import IntegrityError

from account.models import User


class UserTest(TestCase):

    def setUp(self):
        self.user = User(
            email='john@doh.com'
        )
        self.user.save()

    def test_create(self):
        'User may be saved'
        self.assertEqual(1, self.user.pk)

    def test_str(self):
        'User string representation may be the email.'
        self.assertEqual('john@doh.com', str(self.user))

    def test_unique_email(self):
        'Email should be unique'
        u = User(
            email='john@doh.com'
        )
        self.assertRaises(IntegrityError, u.save)