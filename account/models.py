# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, Group
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, first_name, last_name, email, password):
        if not email:
            raise ValueError('')

        user = self.model(
            first_name=first_name,
            last_name=last_name,
            email=UserManager.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, first_name, last_name, email, password):
        user = self.create_user(first_name, last_name, email, password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    # settings
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    # fields
    date_joined = models.DateTimeField(_('cadastrado em'), auto_now_add=True, editable=False)
    email = models.EmailField(_('email'), db_index=True, max_length=255, unique=True)
    first_name = models.CharField(_('nome'), max_length=100)
    is_active = models.BooleanField(
        _('ativo'),
        default=True,
        help_text=_(
            'Indica que o usuário será tratado como ativo.'
            'Ao invés de excluir contas de usuário, desmarque isso.'
        ),
    )
    is_admin = models.BooleanField(_('administrador'), default=False)
    is_staff = models.BooleanField(
        _('funcionário'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    last_name = models.CharField(_('sobrenome'), max_length=100)
    modified = models.DateTimeField(_('alterado em'), auto_now=True, editable=False)

    # default objects class
    objects = UserManager()

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    get_full_name.short_description = 'Nome Completo'

    def get_short_name(self):
        return self.first_name

    def __unicode__(self):
        return self.email

    class Meta:
        ordering = ('first_name', 'last_name', )
        verbose_name = _(u'usuário')
        verbose_name_plural = _(u'usuários')


class UserGroup(Group):
    class Meta:
        ordering = ['name']
        proxy = True
        verbose_name = _('grupo')
        verbose_name_plural = _('grupos')
