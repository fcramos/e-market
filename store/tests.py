from django.test import TestCase

from location.models import State, City
from product.models import Brand, Category, Product
from store.models import Store, Stock


class StoreTestCase(TestCase):
    def setUp(self):
        state = State(
            name='Rio de Janeiro'
        )
        state.save()

        city = City(
            name='Petrópolis',
            state=state
        )
        city.save()

        self.store = Store(
            name='Super Market',
            slug='super-market',
            address='Rua John Doh',
            number='190',
            complement='ABC',
            city=city,
            district='Anywhere',
            postal_code='12345-678'
        )
        self.store.save()

    def test_create(self):
        'Store may be saved'
        self.assertEqual(1, self.store.pk)


class StockTestCase(TestCase):
    def setUp(self):
        state = State(
            name='Rio de Janeiro'
        )
        state.save()

        city = City(
            name='Petrópolis',
            state=state
        )
        city.save()

        store = Store(
            name='Super Market',
            slug='super-market',
            address='Rua John Doh',
            number='190',
            complement='ABC',
            city=city,
            district='Anywhere',
            postal_code='12345-678'
        )
        store.save()

        brand = Brand(
            name='Ajax'
        )
        brand.save()

        category = Category(
            name='General'
        )
        category.save()

        product = Product(
            name='Some product',
            slug='some-product',
            brand=brand,
            category=category,
            unity='PÇ',
            price=12.99
        )
        product.save()

        self.stock = Stock(
            store=store,
            product=product,
            quantity=15
        )
        self.stock.save()

    def test_create(self):
        'Stock may be saved'
        self.assertEqual(1, self.stock.pk)
