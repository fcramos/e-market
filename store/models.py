from django.db import models
from django.utils.translation import ugettext_lazy as _


class Store(models.Model):
    name = models.CharField(_('nome'), max_length=60)
    slug = models.SlugField(_('slug'))
    address = models.CharField(_('endereço'), max_length=80)
    number = models.CharField(_('número'), max_length=80)
    complement = models.CharField(_('complemento'), max_length=80, blank=True, null=True)
    city = models.ForeignKey('location.City', verbose_name=_('cidade'))
    district = models.CharField(_('bairro'), max_length=60)
    postal_code = models.CharField(_('cep'), blank=True, max_length=10, null=True)

    class Meta:
        ordering = ('name', )
        verbose_name = _('loja')
        verbose_name_plural = _('lojas')

    def __str__(self):
        return self.name


class Stock(models.Model):
    store = models.ForeignKey(Store, verbose_name=_('loja'))
    product = models.ForeignKey('product.Product', verbose_name=_('produto'))
    quantity = models.PositiveIntegerField(_('quantidade'), default=0)

    class Meta:
        ordering = ['store', 'product']
        verbose_name = _('Estoque')
        verbose_name_plural = _('Estoques')
        unique_together = (('store', 'product'),)

    def __str__(self):
        return '{} {}'.format(self.product, self.store)
