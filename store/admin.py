from django.contrib import admin

from .models import Store, Stock
from .forms import StoreForm


class StockInline(admin.TabularInline):
    model = Stock
    extra = 1
    fields = ['product', 'quantity']


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'district', 'city']
    search_fields = ['name']
    form = StoreForm
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {'fields': (
            ('name', 'slug'),
        )}),
        ('Endereço', {'fields': (
            ('postal_code', 'address'),
            ('number', 'complement'),
            ('district', 'state', 'city')
        )}),
    )
    inlines = [StockInline]
