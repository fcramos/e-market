from django import forms

from .models import Store
from location.forms import LocationForm


class StoreForm(LocationForm, forms.ModelForm):
    class Meta:
        model = Store
        exclude = []
