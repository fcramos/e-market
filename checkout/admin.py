from django.contrib import admin
from django.utils.translation import ungettext, ugettext_lazy as _

from .models import Cart, CartItem


class CartItemInline(admin.TabularInline):
    model = CartItem
    fields = ['product', 'quantity']
    extra = 0


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ['store', 'customer', 'delivery_date', 'delivery_method', 'created', 'qtd_itens', 'status']
    inlines = [CartItemInline]
    search_fields = ['id']
    list_filter = ['status', 'store', 'created', 'delivery_method', 'delivery_date']
    fieldsets = (
        (None, {'fields': (
            'customer',
            'store',
            'status'
        )}),
        (_('Entrega'), {'fields': (
            'delivery_date',
            'delivery_method',
            'delivery_address'
        )}),
    )

    def qtd_itens(self, instance):
        return instance.cartitem_set.count()

    actions = ['mark_as_delivered']

    def mark_as_delivered(self, request, queryset):
        count = queryset.update(status=Cart.DELIVERED)

        msg = ungettext(
            u'%d carrinho foi marcado como entegue,',
            u'%d carrinhos foram marcadas como entegues.',
            count
        )
        self.message_user(request, msg % count)

    mark_as_delivered.short_description = _(u'Marcar como entregue')
