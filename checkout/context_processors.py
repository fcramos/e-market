from .models import Cart


def get_cart(request):
    try:
        cart = Cart.objects.get(pk=request.COOKIES.get('cart'))
    except Cart.DoesNotExist:
        cart = Cart()

    return {'cart': cart}
