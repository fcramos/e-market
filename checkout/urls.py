from django.conf.urls import url

from .views import add_item, CartView, DeliveryView, ConfirmationView

urlpatterns = [
    url(r'^add-item', add_item, name='add_item'),
    url(r'^cart', CartView.as_view(), name='cart'),
    url(r'^delivery', DeliveryView.as_view(), name='delivery'),
    url(r'^confirmation', ConfirmationView.as_view(), name='confirmation'),
]
