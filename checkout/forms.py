from django import forms

from .models import Cart, CartItem


class CartForm(forms.ModelForm):
    class Meta:
        model = Cart
        fields = ['delivery_method', 'delivery_address', 'delivery_date']


class CartItemForm(forms.ModelForm):
    class Meta:
        model = CartItem
        fields = ['quantity']


CartItemFormSet = forms.inlineformset_factory(
    parent_model=Cart,
    model=CartItem,
    form=CartItemForm,
    extra=0,
)
