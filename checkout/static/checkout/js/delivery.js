$(document).ready(function(){

  $('#id_state').change(function(){
    $.get('/location/cities', {state: this.value}, function(data){
      $('#id_city').empty();
      $.each(data.results, function(index, value){
        $('#id_city').append($('<option/>').val(value.id).html(value.name));
      })
    });
  });

  $('form.modal-content').submit(function(event){
    event.preventDefault();

    $.ajax({
      method: this.method,
      url: this.action,
      data: $(this).serialize(),
      success: function(data){
        $('#modal').modal('hide');
        alertify.success('Endereço salvo com sucesso');

        $('#addresses').append(
          '<div class="col-md-6">'+
          '  <input type="radio" name="delivery_address" value="'+data.results.id+'"> Modificar'+
          '  <p>'+[data.results.address, data.results.number, data.results.complement].join(' ')+
          '  <br>'+[data.results.district, data.results.city, data.results.state].join(' - ')+'</p>'+
          '</div>'
        );
      }
    });
  });
});
