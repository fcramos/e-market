$(document).ready(function(){

  var line;

  $('form.panel-body').submit(function(evt){
    evt.preventDefault();

    $.ajax({
      method: this.method,
      url: '',
      data: $(this).serialize(),
      success: function(data){
        $('#total').html('R$ '+ data.results.total);

        if(line){
          line.remove();
          line = null;
          alertify.success('Produdo removido com sucesso');

          $('#id_cartitem_set-TOTAL_FORMS').val(data.results.items.length);
          $('#id_cartitem_set-INITIAL_FORMS').val(data.results.items.length);
          $('#items').html(data.results.items.length);

          $('.row.item').each(function(index){
            $(this).find('[name]').each(function(){
              $(this).attr({
                id: this.id.replace(/\d/, index),
                name: this.name.replace(/\d/, index)
              });
            });
            $(this).find('p[id^=id_cartitem_set-]').attr('id', 'id_cartitem_set-'+index+'-value');
          });
        }

        $.each(data.results.items, function(index, item){
          $('#id_cartitem_set-'+index+'-value').html('R$ '+item.value);
        });

      }
    });
  });

  $('[id^="id_cartitem_set"][id$=quantity]').change(function(){
    $('form.panel-body').submit();
  });

  $('[id^="id_cartitem_set"][id$=DELETE]').change(function(){
    line = $(this).parents('.row.item');
    alertify.confirm('Confirmação', 'Deseja remover o produto?',
      function(){
        $('form.panel-body').submit();
      },
      function(){
        button.click();
        line = null;
      }).set('labels', {ok:'Sim!', cancel:'Não!'});
  });
});
