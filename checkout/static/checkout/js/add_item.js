$(document).ready(function(){
    $('form.row').submit(function(evt){
        evt.preventDefault();
        form = $(this);
        $.ajax({
            url: this.action,
            type: 'POST',
            data: form.serialize(),
            success: function(data){
                alertify.success('Produto adicionado ao carinho');
                $('#items').html(parseInt($('#items').html())+1);
                form.replaceWith('<h4>Produto no carrinho</h4>');
            },
            error: function(data){
                alertify.warning(data.responseText);
            }
        })

    });
});