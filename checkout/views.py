from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect, Http404
from django.shortcuts import render
from django.views.generic import View, UpdateView
from django.core.urlresolvers import reverse_lazy

from customer.models import Customer
from customer.forms import AddressForm
from .models import Cart
from .forms import CartItemFormSet


def add_item(request):
    response = HttpResponse()
    try:
        cart = Cart .objects.get(pk=request.COOKIES.get('cart'))

        if cart.store_id != int(request.COOKIES.get('store')):
            response.status_code = 400
            response.content = 'Não é possível adcionar produto de outra loja'

            return response

    except Cart.DoesNotExist:
        cart = Cart.objects.create(
            store_id=request.COOKIES.get('store')
        )
        response.set_cookie('cart', cart.pk)

    if request.user.is_authenticated:
        try:
            cart.customer = request.user.customer
            cart.save()
        except:
            pass

    try:
        cart.cartitem_set.create(
            product_id=request.POST.get('product'),
            quantity=request.POST.get('quantity')
        )
    except IntegrityError:
        response.status_code = 400
        response.content = 'O carrinho já possui este produto'

    return response


class CartView(View):
    request = None
    formset = CartItemFormSet

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        return super(CartView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        try:
            return Cart.objects.get(pk=self.request.COOKIES.get('cart'))
        except Cart.DoesNotExist:
            return Cart()

    def get_context(self):
        cart = self.get_object()
        return {
            'object': cart,
            'formset': self.formset(instance=cart)
        }

    def get(self, request):
        return render(request, 'checkout/cart.pug', self.get_context())

    def post(self, request):
        formset = self.formset(request.POST, instance=self.get_object())

        formset.save()

        return JsonResponse({
            'results': {
                'total': formset.instance.total,
                'items': [{'value': item.value} for item in formset.instance.cartitem_set.all()]
            }
        })


class DeliveryView(LoginRequiredMixin, UpdateView):
    model = Cart
    template_name = 'checkout/delivery.pug'
    fields = ['delivery_method', 'delivery_address', 'delivery_date']
    success_url = reverse_lazy('checkout:confirmation')

    def get_object(self):
        try:
            return self.model.objects.get(pk=self.request.COOKIES.get('cart'))
        except Cart.DoesNotExist:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(DeliveryView, self).get_context_data(**kwargs)

        try:
            Customer.objects.get(user_ptr=self.request.user)
        except Customer.DoesNotExist:
            Customer(
                user_ptr=self.request.user,
                date_joined=self.request.user.date_joined,
            ).save_base(raw=True)

        context.update({
            'addresses': self.request.user.customer.address_set.all(),
            'address_form': AddressForm()
        })

        return context

    def form_valid(self, form):
        self.object = form.save()
        self.object.status = Cart.PENDING

        # Updating store stock
        for item in self.object.cartitem_set.all():
            stock = self.object.store.stock_set.get(product=item.product)
            stock.quantity -= item.quantity
            stock.save()

        self.object.save()

        return HttpResponseRedirect(self.success_url)


class ConfirmationView(View):
    def get(self, request):
        response = render(request, 'checkout/confirmation.pug', {})
        response.delete_cookie('cart')

        return response
