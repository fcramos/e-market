import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _

from customer.models import Customer, Address
from product.models import Product
from store.models import Store


class Cart(models.Model):
    DELIVERY = 'D'
    CUSTOMER = 'C'
    DELIVERY_METHOD = (
        (DELIVERY, _('Entrega')),
        (CUSTOMER, _('Retirada'))
    )
    OPEN = 'O'
    PENDING = 'P'
    DELIVERED = 'D'
    STATUS = (
        (OPEN, _('Aberto')),
        (PENDING, _('Aguardando')),
        (DELIVERED, _('Entregue'))
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(Customer, verbose_name=_('cliente'), blank=True, null=True)
    store = models.ForeignKey(Store, verbose_name=_('loja'))
    status = models.CharField(_('situação'), max_length=1, choices=STATUS, default=OPEN)
    delivery_method = models.CharField(_('forma de entrega'), max_length=1, choices=DELIVERY_METHOD, default=CUSTOMER)
    delivery_address = models.ForeignKey(Address, on_delete=models.SET_NULL, verbose_name=_('endereço de entrega'), blank=True, null=True)
    delivery_date = models.DateField(_('Data de entrega'), blank=True, null=True)
    created = models.DateField(_('data de criação'), auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = _('Carrinho')
        verbose_name_plural = _('Carrinhos')

    def __str__(self):
        return 'C-{}'.format(str(self.pk)[:8])

    @property
    def total(self):
        return sum([item.value for item in self.cartitem_set.all()])


class CartItem(models.Model):
    cart = models.ForeignKey(Cart)
    quantity = models.PositiveIntegerField(verbose_name=_('quantidade'), default=0)
    product = models.ForeignKey(Product, verbose_name=_('produto'))

    class Meta:
        ordering = ['product']
        unique_together = (('cart', 'product'), )
        verbose_name = _('Item')
        verbose_name_plural = _('Itens')

    def __str__(self):
        return self.product.name

    @property
    def value(self):
        return self.product.price * self.quantity

    def max(self):
        return self.cart.store.stock_set.get(product=self.product).quantity
