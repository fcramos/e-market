"""emarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.contrib.auth.views import login, logout

from .views import HomeView, StoreView, CustomerCreateView, CustomerUpdateView, ChangePasswordView
from location import urls as location
from checkout import urls as checkout
from customer import urls as customer


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^location/', include(location)),
    url(r'^checkout/', include(checkout, namespace='checkout')),
    url(r'^customer/', include(customer, namespace='customer')),

    url(r'^login$', login, {'template_name': 'site/login.pug'}, name='login'),
    url(r'^logout$', logout, {'next_page': '/'}, name='logout'),
    url(r'^register', CustomerCreateView.as_view(), name='register'),
    url(r'^account', CustomerUpdateView.as_view(), name='account_update'),
    url(r'^change-password', ChangePasswordView.as_view(), name='change_password'),

    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^(?P<slug>[\w-]+)$', StoreView.as_view(), name='store'),
]

if settings.DEBUG:
    urlpatterns.append(
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT})
    )
