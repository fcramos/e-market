$(document).ready(function(){
  $.datepicker.setDefaults({
    beforeShowDay: function(date) {
        var day = date.getDay();
        return [(day != 0 && day != 6), ''];
    },
    dateFormat: "dd/mm/yy",
    altFormat: "yy-mm-dd",
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    minDate: '+1d'
  });

  $('.datepicker').datepicker();
});
