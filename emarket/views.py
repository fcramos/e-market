from django.http.response import Http404
from django.views.generic import CreateView, UpdateView, ListView
from django_filters.views import FilterView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from checkout.models import Cart
from customer.models import Customer
from customer.forms import CustomerCreateForm, ChangePasswordForm
from product.models import Product
from product.filters import ProductFilter
from store.models import Store


class HomeView(ListView):
    model = Store
    template_name = 'site/index.pug'


class StoreView(FilterView):
    template_name = 'site/store.pug'
    paginate_by = 28
    filterset_class = ProductFilter

    def get(self, request, *args, **kwargs):
        response = super(StoreView, self).get(request, *args, **kwargs)
        response.set_cookie('store', self.get_object().pk)

        return response

    def get_object(self):
        try:
            return Store.objects.get(slug=self.kwargs.get('slug'))
        except Store.DoesNotExist:
            raise Http404

    def get_queryset(self):
        queryset = Product.objects.filter(
            pk__in=self.get_object().stock_set.filter(quantity__gt=0).values_list('product_id')
        )

        return queryset

    def get_context_data(self, **kwargs):

        context = super(StoreView, self).get_context_data(**kwargs)
        store = self.get_object()

        context.update({
            'store': store
        })

        try:
            cart = Cart.objects.get(pk=self.request.COOKIES.get('cart'))
        except Cart.DoesNotExist:
            cart = None

        for index, product in enumerate(context.get('object_list')):
            setattr(
                product,
                'quantity',
                product.stock_set.get(store=store).quantity
            )
            if cart:
                setattr(
                    product,
                    'in_cart',
                    cart.cartitem_set.filter(product=product)
                )

        page_obj = context.get('page_obj')

        page_link = '/?{}'.format('&'.join(
            ['='.join([key, value]) for key, value in (
                self.filterset.form.data or {'page': str(page_obj.number)}
            ).items()]
        ))

        if page_obj.has_previous():
            context.update({
                'first_page': page_link.replace(
                    'page={}'.format(page_obj.number),
                    'page=1'
                ),
                'previous_page': page_link.replace(
                    'page={}'.format(page_obj.number),
                    'page={}'.format(page_obj.previous_page_number())
                )
            })

        if page_obj.has_next():
            context.update({
                'next_page': page_link.replace(
                    'page={}'.format(page_obj.number),
                    'page={}'.format(page_obj.next_page_number())
                ),
                'last_page':  page_link.replace(
                    'page={}'.format(page_obj.number),
                    'page={}'.format(page_obj.paginator.num_pages)
                )
            })

        return context


class CustomerCreateView(SuccessMessageMixin, CreateView):
    model = Customer
    form_class = CustomerCreateForm
    template_name = 'customer/customer_form.pug'
    success_message = _('Conta criada com sucesso')
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        """
        Autentica o usuário após o cadastro
        """
        new_user = authenticate(
            username=form.cleaned_data['email'],
            password=form.cleaned_data['password'],
        )

        login(self.request, new_user)

        return super(CustomerCreateView, self).form_valid(form)


class CustomerUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Customer
    template_name = 'customer/customer_form.pug'
    fields = ['first_name', 'last_name', 'phone', 'email', 'type', 'document']
    success_url = reverse_lazy('account_update')
    success_message = _('Conta atualizada com sucesso')

    def get_object(self, queryset=None):
        try:
            return self.request.user.customer
        except Customer.DoesNotExist:
            # Adiciona o registro do usuário na tabela de clientes
            customer = Customer(user_ptr=self.request.user)
            customer.save_base(raw=True)
            return customer


class ChangePasswordView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'customer/change_password.pug'
    success_url = reverse_lazy('account_update')
    form_class = ChangePasswordForm
    success_message = _('Senha alterada com sucesso')

    def get_object(self, queryset=None):
        return self.request.user
