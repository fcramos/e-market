from django.conf.urls import url

from .views import AddressView

urlpatterns = [
    url(r'^address/(?:(?P<pk>\d+)/)?$', AddressView.as_view(), name='address'),
]
