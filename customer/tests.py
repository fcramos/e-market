from django.test import TestCase
from django.db import IntegrityError

from customer.models import Customer, Address
from location.models import City, State


class CustomerTest(TestCase):
    def setUp(self):
        self.customer = Customer(
            email='john@doh.com',
            document='12345678'
        )
        self.customer.save()

    def test_create(self):
        'Customer may be saved'
        self.assertEqual(1, self.customer.pk)

    def test_str(self):
        'Customer string representation may be the email.'
        self.assertEqual('john@doh.com', str(self.customer))

    def test_unique_document(self):
        'Document should be unique'
        c = Customer(
            email='john@email.com',
            document='12345678'
        )
        self.assertRaises(IntegrityError, c.save)


class AddressTestCase(TestCase):
    def setUp(self):
        state = State(
            name='Rio de Janeiro'
        )
        state.save()

        city = City(
            name='Petrópolis',
            state=state
        )
        city.save()

        customer = Customer(
            email='john@doh.com',
            document='12345678'
        )
        customer.save()

        self.address = Address(
            customer=customer,
            address='Rua John Doh',
            number='190',
            complement='ABC',
            city=city,
            district='Anywhere',
            postal_code='12345-678'
        )
        self.address.save()

    def test_create(self):
        'Address may be saved'
        self.assertEqual(1, self.address.pk)
