from django.db import models
from django.utils.translation import ugettext_lazy as _
from account.models import User, UserManager


class Customer(User):
    TYPES = (
        ('F', _('Física')),
        ('J', _('Jurídica'))
    )
    type = models.CharField(_('tipo'), max_length=1, choices=TYPES, default='F')
    document = models.CharField(_('CPF/CNPJ'), max_length=18, unique=True, null=True)
    phone = models.CharField(_('telefone'), blank=True, max_length=15, null=True)

    objects = UserManager()

    class Meta:
        ordering = ('first_name', 'last_name', )
        verbose_name = _('Cliente')
        verbose_name_plural = _('Clientes')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.is_admin = False
            self.is_superuser = False
            self.is_staff = False

        return super(Customer, self).save(*args, **kwargs)


class Address(models.Model):
    customer = models.ForeignKey(Customer)
    address = models.CharField(_('endereço'), max_length=80)
    number = models.CharField(_('número'), max_length=80)
    complement = models.CharField(_('complemento'), max_length=80, blank=True, null=True)
    city = models.ForeignKey('location.City', verbose_name=_('cidade'))
    district = models.CharField(_('bairro'), max_length=60)
    postal_code = models.CharField(_('cep'), blank=True, max_length=10, null=True)

    class Meta:
        ordering = ('-id', )
        verbose_name = _(u'endereço')
        verbose_name_plural = _(u'endereços')
