from django.http.response import JsonResponse, Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import View

from .models import Address
from .forms import AddressForm


class AddressView(View):
    def get(self, request, pk=None):
        address = get_object_or_404(Address, pk=pk)

        return JsonResponse({'results': {
            'postal_code': address.postal_code,
            'address': address.address,
            'number': address.number,
            'complement': address.complement,
            'district': address.district,
            'city': address.city.pk
        }})

    def post(self, request, pk=None):
        if pk:
            form = AddressForm(request.POST, instance=Address.objects.get(pk=pk))
        else:
            form = AddressForm(request.POST)

        address = form.save()

        return JsonResponse({'results': {
            'id': address.pk,
            'postal_code': address.postal_code,
            'address': address.address,
            'number': address.number,
            'complement': address.complement,
            'district': address.district,
            'city': address.city.name,
            'state': address.city.state.initials,
        }})

    def delete(self, request, pk=None):
        try:
            Address.objects.get(pk=pk).delete()
        except Address.DoesNotExist:
            raise Http404

        return HttpResponse()
