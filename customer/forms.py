from django import forms
from django.utils.translation import ugettext_lazy as _

from location.forms import LocationForm
from .models import Customer, Address


class PasswordForm(forms.Form):
    password_confirm = forms.CharField(
        label=_('Confirme a senha'),
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )

    def clean_password_confirm(self):
        if self.cleaned_data['password_confirm'] != self.data['password']:
            raise forms.ValidationError(_(u'confirmação de senha não confere'))

    def save(self, commit=True):
        password = self.cleaned_data['password']
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user


class CustomerCreateForm(forms.ModelForm, PasswordForm):
    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'phone', 'email', 'type', 'document', 'password']


class ChangePasswordForm(forms.ModelForm):
    old_password = forms.CharField(
        label=_('Senha atual'),
        widget=forms.PasswordInput
    )
    new_password = forms.CharField(
        label=_('Nova senha'),
        widget=forms.PasswordInput
    )
    confirm_password = forms.CharField(
        label=_('Confirmação da nova senha'),
        widget=forms.PasswordInput
    )

    class Meta:
        model = Customer
        fields = []

    def clean_old_password(self):
        if not self.instance.check_password(self.cleaned_data['old_password']):
            raise forms.ValidationError(_(u'senha atual não confere'))

    def clean_confirm_password(self):
        if self.cleaned_data['new_password'] != self.data['confirm_password']:
            raise forms.ValidationError(_(u'confirmação de senha não confere'))

    def save(self, commit=True):
        password = self.cleaned_data['new_password']
        self.instance.set_password(password)

        return super(ChangePasswordForm, self).save(commit)


class AddressForm(forms.ModelForm, LocationForm):
    class Meta:
        model = Address
        exclude = []
