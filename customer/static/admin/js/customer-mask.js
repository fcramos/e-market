var phoneMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
};

var phoneOptions = {
  onKeyPress: function(val, e, field, options) {
    field.mask(phoneMaskBehavior.apply({}, arguments), options);
  }
};

$(document).ready(function(){
  $('#id_phone').mask(phoneMaskBehavior, phoneOptions);

  var typeMask = {
    'F': {'label': 'CPF:', 'mask': '999.999.999-99'},
    'J': {'label': 'CNPJ:', 'mask': '99.999.999/9999-99'}
  }

  $('#id_type').change(function(){
    $('[for="id_document"]').html(typeMask[this.value]['label']);
    $('#id_document').val('').change();
    $('#id_document').mask(typeMask[this.value]['mask']);
  });

  if(!$('#id_document').val()){
    $('#id_type').change();
  }
})