from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from account.admin import UserAdmin
from customer.models import Customer, Address


class AddressInline(admin.StackedInline):
    model = Address
    extra = 1
    fieldsets = (
        (None, {'fields': (
            ('postal_code', 'address'),
            ('number', 'complement'),
            ('district', 'city')
        )}),
    )


@admin.register(Customer)
class CustomerAdmin(UserAdmin):
    list_display = ('get_full_name', 'type', 'email')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Informações pessoais'), {'fields': (
            ('first_name', 'last_name'),
            ('type', 'document'),
            ('phone',)
        )}),
        (_('Permissões'), {'fields': ('is_active',)}),
        (_('Datas importantes'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    def get_list_filter(self, request):
        filter_list = super(CustomerAdmin, self).get_list_filter(request)
        filter_list = filter_list + ('type', )

        return filter_list

    inlines = [AddressInline]

    class Media:
        js = (
            '/static/admin/js/fix-jquery.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js',
            '/static/admin/js/customer-mask.js',
        )

    def get_inline_instances(self, request, obj=None):
        if obj:
            return super(CustomerAdmin, self).get_inline_instances(request, obj)
        else:
            return []
